import cv2
import numpy as np
import math


def assemble_strokes(x, y, lens):
    strokes = []
    offset = 0
    for i, l in enumerate(lens):
        strokes.append([x[offset:offset + l], y[offset:offset + l]])
        offset += l
    return strokes


def partition_strokes(strokes, num_partitions):
    total_num_points = sum([len(s[0]) for s in strokes])
    partition_num_points = math.ceil(total_num_points / num_partitions)

    partitions = []

    current_partition = []
    current_partition_points = 0
    for s, stroke in enumerate(strokes):
        current_partition.append(stroke)
        current_partition_points += len(stroke[0])
        if current_partition_points >= partition_num_points or s == len(strokes) - 1:
            partitions.append(current_partition)
            current_partition = []
            current_partition_points = 0

    for _ in range(len(partitions), num_partitions):
        partitions.append([])

    return partitions


def draw_strokes(strokes, size=256, line_width=7, padding=3, fliplr=False):
    draw_size = 256
    scale_factor = (draw_size - 2 * padding) / draw_size

    image = np.full((draw_size, draw_size), 255, dtype=np.uint8)

    stroke_colors = range(0, 240, 40)

    for s, stroke in enumerate(strokes):
        stroke_color = stroke_colors[s % len(stroke_colors)]
        for i in range(len(stroke[0]) - 1):
            x0 = int(scale_factor * stroke[0][i]) + padding
            y0 = int(scale_factor * stroke[1][i]) + padding
            x1 = int(scale_factor * stroke[0][i + 1]) + padding
            y1 = int(scale_factor * stroke[1][i + 1]) + padding
            if fliplr:
                x0 = draw_size - x0
                x1 = draw_size - x1
            cv2.line(image, (x0, y0), (x1, y1), stroke_color, line_width)

    if draw_size != size:
        image = cv2.resize(image, (size, size), interpolation=cv2.INTER_AREA)

    return image


def merge_stroke_drawings(drawings):
    merged_drawing = np.full(drawings[0].shape, 255, dtype=np.uint8)
    for drawing in drawings:
        merged_drawing[drawing != 255] = drawing[drawing != 255]
    return merged_drawing


def draw_temporal_strokes(strokes, size=256, line_width=7, padding=3, fliplr=False, extended_channels=True):
    draw_size = 256
    scale_factor = (draw_size - 2 * padding) / draw_size

    stroke_colors = range(0, 240, 40)

    partition_images = []

    stroke_partitions = partition_strokes(strokes, 3)
    stroke_color_index = 0
    for stroke_partition in stroke_partitions:
        image = np.full((draw_size, draw_size), 255, dtype=np.uint8)
        partition_images.append(image)

        for stroke in stroke_partition:
            stroke_color = stroke_colors[stroke_color_index % len(stroke_colors)]
            stroke_color_index += 1
            for i in range(len(stroke[0]) - 1):
                x0 = int(scale_factor * stroke[0][i]) + padding
                y0 = int(scale_factor * stroke[1][i]) + padding
                x1 = int(scale_factor * stroke[0][i + 1]) + padding
                y1 = int(scale_factor * stroke[1][i + 1]) + padding
                if fliplr:
                    x0 = draw_size - x0
                    x1 = draw_size - x1
                cv2.line(image, (x0, y0), (x1, y1), stroke_color, line_width)

    if draw_size != size:
        partition_images = [cv2.resize(i, (size, size), interpolation=cv2.INTER_AREA) for i in partition_images]

    final_images = []
    if extended_channels:
        final_images.extend(partition_images)
        final_images.append(merge_stroke_drawings([partition_images[0], partition_images[1]]))
        final_images.append(merge_stroke_drawings([partition_images[1], partition_images[2]]))
        final_images.append(merge_stroke_drawings([partition_images[0], partition_images[1], partition_images[2]]))
    else:
        final_images.append(partition_images[0])
        final_images.append(merge_stroke_drawings([partition_images[0], partition_images[1]]))
        final_images.append(merge_stroke_drawings([partition_images[0], partition_images[1], partition_images[2]]))

    return np.array(final_images)


def calculate_drawing_values_channel(drawing, country, size):
    country_value = country / 255.
    num_strokes_value = len(drawing) / 15.
    stroke_len_value = calculate_mean_stroke_len(drawing) / 80.

    value_stride = size // 3
    values_channel = np.zeros((size, size), dtype=np.float32)
    values_channel[0:value_stride] = country_value
    values_channel[value_stride:2 * value_stride] = num_strokes_value
    values_channel[2 * value_stride:] = stroke_len_value

    return values_channel
