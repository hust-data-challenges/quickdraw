import multiprocessing as mp
import numpy as np
import time
import math
from libs.utils.network import image_to_tensor, category_to_one_hot_tensor, category_to_tensor
from libs.constant import CATEGORIES, COUNTRIES, COUNTRY_INDEX_MAP
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit
from libs.utils.common import kfold_split
import datetime
from torch.utils.data import Dataset, Sampler
import pandas as pd
from torchvision import transforms
from libs.constant import CATEGORIES
import torch


class TrainDataProvider:
    @staticmethod
    def load_data(
            data_dir,
            shard,
            fold,
            test_size,
            train_on_unrecognized):
        print("[{}] Loading data for shard {}".format(mp.current_process().name, shard), flush=True)
        return TrainData(
            data_dir,
            shard,
            fold,
            test_size,
            train_on_unrecognized
        )

    def __init__(
            self,
            data_dir,
            num_shards,
            num_shards_preload,
            num_worker,
            fold,
            test_size,
            train_on_unrecognized):
        self.data_dir = data_dir
        self.num_shards = num_shards
        self.num_shards_preload = num_shards_preload
        self.num_worker = num_worker
        self.fold = fold
        self.test_size = test_size
        self.train_on_unrecognized = train_on_unrecognized == 'True'
        
        self.shards = range(self.num_shards)
        np.random.shuffle(self.shards)

        self.pool = mp.Pool(processes=num_worker)
        self.requests = []

        self.next_shard_index = 0
        for _ in range(num_shards_preload):
            self.request_data()

    def get_next(self):
        start_time = time.time()

        self.request_data()
        data = self.requests.pop(0).get()

        end_time = time.time()
        print("[{}] Time to provide data of shard {}: {}".format(
            mp.current_process().name,
            data.shard,
            str(datetime.timedelta(seconds=end_time - start_time))),
            flush=True)

        return data

    def request_data(self):
        next_shard = self.shards[self.next_shard_index]
        print("[{}] Placing request the shard {}".format(mp.current_process().name, next_shard), flush=True)
        self.requests.append(self.pool.apply_async(
            TrainDataProvider.load_data(
                self.data_dir,
                next_shard,
                self.fold,
                self.test_size,
                self.train_on_unrecognized
            )
        ))


class TrainData:
    base_filename = "/train_simplified_shards/"

    def __init__(
            self,
            data_dir,
            shard,
            fold,
            test_size,
            train_on_unrecognized):
        self.data_dir = data_dir
        self.shard = shard
        self.fold = fold
        self.test_size = test_size
        self.train_on_recognized = train_on_unrecognized
        self.categories = CATEGORIES
        self.countries = COUNTRIES

        start_time = time.time()
        data_filename = data_dir + TrainData.base_filename + "shard-{}.npz".format(shard)
        print("Reading data file {}".format(data_filename), flush=True)

        with np.load(data_filename, allow_pickle=True) as loader:
            data_category = loader["category"]
            data_drawing = loader["drawing"]
            data_recognized = loader["recognized"]
            data_countrycode = loader["countrycode"]

        print("Loaded {} samples".format(len(data_drawing)))
        country_index_map = {c: self.countries.index(c) for c in self.countries}
        data_country = np.array([country_index_map[c] if isinstance(c, str) else -1 for c in data_countrycode], np.uint8)
        if fold is None:
            train_categories, val_categories, train_drawing, \
            val_drawing, train_recognized, val_recognized, train_country, val_country = \
                train_test_split(
                    data_category,
                    data_drawing,
                    data_recognized,
                    data_country,
                    test_size=test_size,
                    stratify=data_category,
                    random_state=42
                )
        else:
            train_indices, val_indices = list(kfold_split(3, len(data_category), data_category))[fold]
            train_categories = data_category[train_indices]
            train_drawing = data_drawing[train_indices]
            train_recognized = data_recognized[train_indices]
            train_country = data_country[train_indices]

            val_categories = data_category[val_indices]
            val_drawing = data_drawing[val_indices]
            val_recognized = data_recognized[val_indices]
            val_country = data_country[val_indices]

        if not train_on_unrecognized:
            train_categories = train_categories[train_recognized]
            train_drawing = train_drawing[train_recognized]
            train_country = train_country[train_recognized]
            train_recognized = train_recognized[train_recognized]

        self.train_set_df = {
            "category": train_categories,
            "drawing": train_drawing,
            "country": train_country,
            "recognized": train_recognized
        }

        self.val_set_df = {
            "category": val_categories,
            "drawing": val_drawing,
            "country": val_country,
            "recognized": val_recognized
        }

        end_time = time.time()
        print("Time to load data of shard {}: {}".format(shard, str(datetime.timedelta(end_time - start_time))),
              flush=True)


class QuickDrawDataset(Dataset):
    def __init__(self, df, num_categories, image_size, use_extended_stroke_channels,
                 draw_strokes, augment, transform=None, use_dummy_image=False):
        self.df = df
        self.transform = transform
        self.draw_strokes = draw_strokes
        self.num_categories = num_categories
        self.image_size = image_size
        self.use_extended_stroke_channels = use_extended_stroke_channels
        self.augment = augment
        self.use_dummy_image = use_dummy_image

    def __len__(self):
        return len(self.df["drawing"])

    def __getitem__(self, index):
        drawing = self.df["drawing"][index]
        category = self.df["category"][index]
        # country = self.df["country"][index]

        if self.use_dummy_image:
            image = np.zeros(self.image_size, self.image_size)
        elif "image" in self.df.index:
            image = self.df["image"][index]
        else:
            fliplr = False
            padding = 3

            if self.augment:
                if self.augment:
                    if np.random.rand() < 0.5:
                        fliplr = True
                        # if np.random.rand() < 0.2:
                        #     padding += np.random.randint(5, 50)

            image = self.draw_strokes(
                drawing,
                size=self.image_size,
                padding=padding,
                fliplr=fliplr,
                extended_channels=self.use_extended_stroke_channels)

        image = image_to_tensor(image)
        category = category_to_tensor(category)
        category_one_hot = category_to_one_hot_tensor(category, self.num_categories)

        if self.transform:
            image = self.transform(image)
        # values_channel = calculate_drawing_values_channel(drawing, country, self.image_size)
        # image = torch.cat([torch.from_numpy(values_channel).float().unsqueeze(0), image], dim=0)

        # image = normalize(image, (0.485, 0.456, 0.406), (0.229, 0.224, 0.225))

        return image, category, category_one_hot

class TestData:
    def __init__(self, data_dir):
        self.df = pd.read_csv(
            "{}/test_simplified.csv".format(data_dir),
            index_col="key_id",
            converters={"drawing": lambda drawing: eval(drawing)})

        # countries = read_lines("{}/countries.txt".format(data_dir))
        self.df["country"] = [CATEGORIES.index(c) if isinstance(c, str) else 255 for c in self.df.countrycode]


class TestDataset(Dataset):
    def __init__(self, df, image_size, draw_strokes, use_extended_stroke_channels):
        super().__init__()
        self.df = df
        self.draw_strokes = draw_strokes
        self.image_size = image_size
        self.use_extended_stroke_channels = use_extended_stroke_channels

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        drawing = self.df.iloc[index].drawing
        country = self.df.iloc[index].country

        image = self.draw_strokes(
            drawing,
            size=self.image_size,
            padding=3,
            extended_channels=self.use_extended_stroke_channels)

        image = image_to_tensor(image)

        # values_channel = calculate_drawing_values_channel(drawing, country, self.image_size)
        # image = torch.cat([torch.from_numpy(values_channel).float().unsqueeze(0), image], dim=0)

        return (image,)


class StratifiedSampler(Sampler):
    def __init__(self, class_vector, batch_size):
        super().__init__(None)
        self.class_vector = class_vector
        self.batch_size = batch_size

    def gen_sample_array(self):
        n_splits = math.ceil(len(self.class_vector) / self.batch_size)
        splitter = StratifiedShuffleSplit(n_splits=n_splits, test_size=0.5)
        train_index, test_index = next(splitter.split(np.zeros(len(self.class_vector)), self.class_vector))
        return np.hstack([train_index, test_index])

    def __iter__(self):
        return iter(self.gen_sample_array())

    def __len__(self):
        return len(self.class_vector)
