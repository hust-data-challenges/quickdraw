from .senet_wrapper import SeNet, SeResNext50Cs
from .resnet import resnet50 as ResNet50
from .stacknet import StackNet
from .ensemble import Ensemble
from libs.utils.network import find_sorted_model_files
import os
import torch.nn as nn
import torch
from libs.modules import *

def create_model(type, input_size, num_classes):
    if type == "resnet":
        model = ResNet50(num_classes=num_classes)
    elif type in ["seresnext50", "seresnext101", "seresnet50", "seresnet101", "seresnet152", "senet154"]:
        model = SeNet(type=type, num_classes=num_classes)
    elif type == "seresnext50_cs":
        model = SeResNext50Cs(num_classes=num_classes)
    elif type == "stack":
        model = StackNet(num_classes=num_classes)
    else:
        raise Exception("Unsupported model type: '{}".format(type))

    return nn.DataParallel(model)


def load_ensemble_model(base_dir, ensemble_model_count, data_loader, criterion, model_type, input_size,
                        num_classes, device):
    ensemble_model_candidates = find_sorted_model_files(base_dir)[-(2 * ensemble_model_count):]
    if os.path.isfile("{}/swa_model.pth".format(base_dir)):
        ensemble_model_candidates.append("{}/swa_model.pth".format(base_dir))

    score_to_model = {}
    for model_file_path in ensemble_model_candidates:
        model_file_name = os.path.basename(model_file_path)
        model = create_model(type=model_type, input_size=input_size, num_classes=num_classes).to(device)
        model.load_state_dict(torch.load(model_file_path, map_location=device))

        val_loss_avg, val_mapk_avg, _, _, _, _ = evaluate(model, data_loader, criterion, 3)
        print("ensemble '%s': val_loss=%.4f, val_mapk=%.4f" % (model_file_name, val_loss_avg, val_mapk_avg))

        if len(score_to_model) < ensemble_model_count or min(score_to_model.keys()) < val_mapk_avg:
            if len(score_to_model) >= ensemble_model_count:
                del score_to_model[min(score_to_model.keys())]
            score_to_model[val_mapk_avg] = model

    ensemble = Ensemble(list(score_to_model.values()))

    val_loss_avg, val_mapk_avg, _, _, _, _ = evaluate(ensemble, data_loader, criterion, 3)
    print("ensemble: val_loss=%.4f, val_mapk=%.4f" % (val_loss_avg, val_mapk_avg))

    return ensemble