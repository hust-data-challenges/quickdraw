import datetime
from glob import glob
import sys
import os
import numpy as np
import time
from math import ceil

import psutil
import torch
import shutil
from libs.utils.network import zero_item_tensor, get_loss_target, get_learning_rate
from libs.datasets.quickdraw import TrainDataProvider, \
    QuickDrawDataset, StratifiedSampler, TestData, TestDataset
from libs.utils.network import adjust_learning_rate, adjust_initial_learning_rate, check_model_improved
from libs.modules import mapk, evaluate, predict, calculate_confusion
from libs.models import create_model, load_ensemble_model, Ensemble
from libs.optimizers import create_optimizer
from torch.utils.data import DataLoader
from libs.lr_schedulers import CosineAnnealingLR, ReduceLROnPlateau
from libs.losses import create_criterion
from tensorboardX import SummaryWriter
from libs.utils.strokes_algorithms import draw_temporal_strokes

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')


class Solver:
    def __init__(self, config):
        self.model_cfg = config.model
        self.train_cfg = config.train
        self.optimizer_cfg = config.train.optimizer
        self.lr_scheduler_cfg = config.train.lr_scheduler
        self.loss_cfg = config.train.loss
        self.dataset_cfg = config.dataset
        self.pharse = config.pharse

        # dataset
        self.use_extended_stroke_channels = self.model_cfg.type in ["cnn", "residual_cnn", "fc_cnn", "hc_fc_cnn"]
        print("use_extended_stroke_channels: {}".format(self.use_extended_stroke_channels), flush=True)
        train_data_provider = TrainDataProvider(
            data_dir=self.dataset_cfg.dataset_dir,
            num_shards=self.dataset_cfg.num_shards,
            num_worker=self.train_cfg.num_worker,
            num_shards_preload=self.dataset_cfg.num_shards_preload,
            fold=self.train_cfg.fold,
            test_size=self.dataset_cfg.test_size,
            train_on_unrecognized=self.train_cfg.train_on_unrecognized
        )
        train_data = train_data_provider.get_next()
        if 'train' in self.pharse:
            train_set = QuickDrawDataset(
                df=train_data.train_set_df,
                draw_strokes=draw_temporal_strokes,
                num_categories=len(train_data.categories),
                image_size=self.model_cfg.image_size,
                use_extended_stroke_channels=self.use_extended_stroke_channels,
                augment=self.train_cfg.augment == 'True'
            )
            stratified_sampler = StratifiedSampler(train_data.train_set_df['category'],
                                                   self.train_cfg.batch_size * self.train_cfg.batch_iterations)
            train_data_loader = DataLoader(train_set, batch_size=self.train_cfg.batch_size, shuffle=False,
                                           sampler=stratified_sampler, num_workers=self.train_cfg.num_worker,
                                           pin_memory=self.train_cfg.pin_memory)
            self.train_data_provider = train_data_provider
            self.train_data = train_data
            self.train_set = train_set
            self.train_data_loader = train_data_loader
            self.stratified_sampler = stratified_sampler

        if 'eval' in self.pharse:
            val_set = QuickDrawDataset(
                df=train_data.val_set_df,
                num_categories=len(train_data.categories),
                image_size=self.model_cfg.image_size,
                draw_strokes=draw_temporal_strokes,
                use_extended_stroke_channels=self.use_extended_stroke_channels,
                augment=False)

            val_data_loader = \
                DataLoader(val_set, batch_size=self.train_cfg.batch_size, shuffle=False,
                           num_workers=self.train_cfg.num_workers, pin_memory=self.train_cfg.pin_memory)
            self.val_set = val_set
            self.val_data_loader = val_data_loader

        if 'test' in self.pharse:
            self.test_data = TestData(self.dataset_cfg.dataset_dir)
            self.test_set = TestDataset(self.test_data.df, self.model.image_size, draw_temporal_strokes,
                                        self.use_extended_stroke_channels)
            self.test_data_loader = \
                DataLoader(self.test_set, batch_size=self.train_cfg.batch_size, shuffle=False,
                           num_workers=self.train_cfg.num_workers, pin_memory=self.train_cfg.pin_memory)

        self.intialize()

        print("train_set_samples: {}, val_set_samples: {}".format(len(self.train_set), len(self.val_set)), flush=True)
        print()

        self.lr_scheduler = CosineAnnealingLR(self.optimizer, T_max=self.optimizer_cfg.sgdr_cycle_epochs,
                                              eta_min=self.lr_scheduler_cfg.lr_min)
        self.lr_scheduler_plateau = ReduceLROnPlateau(self.optimizer, mode="max", min_lr=self.lr_scheduler_cfg.lr_min,
                                                      patience=self.lr_scheduler_cfg.lr_patience, factor=0.8,
                                                      threshold=1e-4)

        self.optim_summary_writer = SummaryWriter(log_dir="{}/logs/optim".format(self.model_cfg.output_dir))
        self.train_summary_writer = SummaryWriter(log_dir="{}/logs/train".format(self.model_cfg.output_dir))
        self.val_summary_writer = SummaryWriter(log_dir="{}/logs/val".format(self.model_cfg.output_dir))

    def intialize(self):
        if self.model_cfg.base_model_dir:
            for base_file_path in glob("{}/*.pth".format(self.model_cfg.base_model_dir)):
                shutil.copyfile(base_file_path,
                                "{}/{}".format(self.train_cfg.output_dir, os.path.basename(base_file_path)))
            model = create_model(type=self.model_cfg.type, input_size=self.model_cfg.image_size,
                                 num_classes=len(self.train_data.categories)).to(device)
            model.load_state_dict(torch.load("{}/model.pth".format(self.train_cfg.output_dir), map_location=device))
            optimizer = create_optimizer(self.optimizer_cfg.type, model, self.lr_scheduler_cfg.lr_max)
            if os.path.isfile("{}/optimizer.pth".format(self.model_cfg.output_dir)):
                optimizer.load_state_dict(torch.load("{}/optimizer.pth".format(self.model_cfg.output_dir)))
                adjust_initial_learning_rate(optimizer, self.lr_scheduler_cfg.lr_max)
                adjust_learning_rate(optimizer, self.lr_scheduler_cfg.lr_max)
        else:
            model = create_model(type=self.model_cfg.type, input_size=self.model_cfg.image_size,
                                 num_classes=len(self.train_data.categories)).to(device)
            optimizer = create_optimizer(self.optimizer_cfg.type, model, self.lr_scheduler_cfg.lr_max)

        criterion = create_criterion(self.loss_cfg.type,
                                     len(self.train_data.categories), self.loss_cfg.bootstraping_loss_ratio)

        if self.loss_cfg.type == "center":
            self.optimizer_centloss = torch.optim.SGD(criterion.center.parameters(), lr=0.01)

        self.model, self.optimizer, self.criterion = model, optimizer, criterion

        # output_dir", default="/artifacts
        self.ensemble_model_index = 0
        for model_file_path in glob("{}/model-*.pth".format(self.model_cfg.base_model_dir)):
            model_file_name = os.path.basename(model_file_path)
            model_index = int(model_file_name.replace("model-", "").replace(".pth", ""))
            self.ensemble_model_index = max(self.ensemble_model_index, model_index + 1)

        if self.train_cfg.confusion_set is not None:
            shutil.copyfile(
                "/storage/models/seresnext50_confusion/confusion_set_{}.txt".
                    format(self.train_cfg.confusion_set),
                "{}/confusion_set.txt".format(self.model_cfg.output_dir))

    def train_model(self, model, optimizer, optimizer_centloss,
                    criterion, lr_scheduler, lr_scheduler_plateau, epochs_to_train):
        epoch_iterations = ceil(len(self.train_set) / self.train_cfg.batch_size)
        current_sgdr_cycle_epochs = self.optimizer_cfg.sgdr_cycle_epochs
        sgdr_next_cycle_end_epoch = current_sgdr_cycle_epochs + self.optimizer_cfg.sgdr_cycle_end_prolongation
        sgdr_iterations = 0
        sgdr_cycle_count = 0
        batch_count = 0
        epoch_of_last_improval = 0

        print('{"chart": "best_val_mapk", "axis": "epoch"}')
        print('{"chart": "val_mapk", "axis": "epoch"}')
        print('{"chart": "val_loss", "axis": "epoch"}')
        print('{"chart": "val_accuracy@1", "axis": "epoch"}')
        print('{"chart": "val_accuracy@3", "axis": "epoch"}')
        print('{"chart": "val_accuracy@5", "axis": "epoch"}')
        print('{"chart": "val_accuracy@10", "axis": "epoch"}')
        print('{"chart": "sgdr_cycle", "axis": "epoch"}')
        print('{"chart": "mapk", "axis": "epoch"}')
        print('{"chart": "loss", "axis": "epoch"}')
        print('{"chart": "lr_scaled", "axis": "epoch"}')
        print('{"chart": "mem_used", "axis": "epoch"}')
        print('{"chart": "epoch_time", "axis": "epoch"}')

        global_val_mapk_best_avg = float("-inf")
        sgdr_cycle_val_mapk_best_avg = float("-inf")

        train_start_time = time.time()

        for epoch in range(epochs_to_train):
            epoch_start_time = time.time()

            print("memory used: {:.2f} GB".format(psutil.virtual_memory().used / 2 ** 30), flush=True)
            model.train()

            train_loss_sum_t = zero_item_tensor(device)
            train_mapk_sum_t = zero_item_tensor(device)

            epoch_batch_iter_count = 0

            for b, batch in enumerate(self.train_data_loader):
                images, categories, categories_one_hot = \
                    batch[0].to(device, non_blocking=True), \
                    batch[1].to(device, non_blocking=True), \
                    batch[2].to(device, non_blocking=True)

                if self.lr_scheduler_cfg.type == "cosine_annealing":
                    lr_scheduler.step(epoch=min(current_sgdr_cycle_epochs, sgdr_iterations / epoch_iterations))

                if b % self.train_cfg.batch_iterations == 0:
                    optimizer.zero_grad()

                prediction_logits = model(images)
                # if prediction_logits.size(1) == len(class_weights):
                #     criterion.weight = class_weights
                loss = criterion(prediction_logits, get_loss_target(criterion, categories, categories_one_hot))
                loss.backward()

                with torch.no_grad():
                    train_loss_sum_t += loss
                    if True:
                        train_mapk_sum_t += mapk(prediction_logits, categories,
                                                 topk=min(self.train_cfg.mapk_topk, len(self.train_data.categories)))

                if (b + 1) % self.train_cfg.batch_iterations == 0 or (b + 1) == len(self.train_data_loader):
                    optimizer.step()
                    if self.loss_cfg.type == "center":
                        for param in criterion.center.parameters():
                            param.grad.data *= (1. / 0.5)
                        optimizer_centloss.step()

                sgdr_iterations += 1
                batch_count += 1
                epoch_batch_iter_count += 1

                self.optim_summary_writer.add_scalar("lr", get_learning_rate(optimizer), batch_count + 1)

            train_data = self.train_data_provider.get_next()
            self.train_set.df = train_data.train_set_df
            self.val_set.df = train_data.val_set_df
            epoch_iterations = ceil(len(self.train_set) / self.train_cfg.batch_size)
            self.stratified_sampler.class_vector = train_data.train_set_df["category"]

            train_loss_avg = train_loss_sum_t.item() / epoch_batch_iter_count
            train_mapk_avg = train_mapk_sum_t.item() / epoch_batch_iter_count

            val_loss_avg, val_mapk_avg, val_accuracy_top1_avg, val_accuracy_top3_avg, val_accuracy_top5_avg, \
            val_accuracy_top10_avg = evaluate(model, self.val_data_loader,
                                              criterion, self.train_cfg.mapk_topk, device=device)

            if self.lr_scheduler_cfg.type == "reduce_on_plateau":
                lr_scheduler_plateau.step(val_mapk_avg)

            model_improved_within_sgdr_cycle = check_model_improved(sgdr_cycle_val_mapk_best_avg, val_mapk_avg)
            if model_improved_within_sgdr_cycle:
                torch.save(model.state_dict(),
                           "{}/model-{}.pth".format(self.model_cfg.output_dir, self.ensemble_model_index))
                sgdr_cycle_val_mapk_best_avg = val_mapk_avg

            model_improved = check_model_improved(global_val_mapk_best_avg, val_mapk_avg)
            ckpt_saved = False
            if model_improved:
                torch.save(model.state_dict(), "{}/model.pth".format(self.model_cfg.output_dir))
                torch.save(optimizer.state_dict(), "{}/optimizer.pth".format(self.model_cfg.output_dir))
                global_val_mapk_best_avg = val_mapk_avg
                epoch_of_last_improval = epoch
                ckpt_saved = True

            sgdr_reset = False
            if (self.lr_scheduler_cfg == "cosine_annealing") and (epoch + 1 >= sgdr_next_cycle_end_epoch) and (
                    epoch - epoch_of_last_improval >= self.optimizer_cfg.sgdr_cycle_end_patience):
                sgdr_iterations = 0
                current_sgdr_cycle_epochs = \
                    int(current_sgdr_cycle_epochs * self.optimizer_cfg.sgdr_cycle_epochs_mult)
                sgdr_next_cycle_end_epoch = \
                    epoch + 1 + current_sgdr_cycle_epochs + self.optimizer_cfg.sgdr_cycle_end_prolongation

                self.ensemble_model_index += 1
                sgdr_cycle_val_mapk_best_avg = float("-inf")
                sgdr_cycle_count += 1
                sgdr_reset = True

                new_lr_min = self.lr_scheduler_cfg.lr_min * (self.lr_scheduler_cfg.lr_min_decay ** sgdr_cycle_count)
                new_lr_max = self.lr_scheduler_cfg.lr_max * (self.lr_scheduler_cfg.lr_max_decay ** sgdr_cycle_count)
                new_lr_max = max(new_lr_max, new_lr_min)

                adjust_learning_rate(optimizer, new_lr_max)
                lr_scheduler = CosineAnnealingLR(optimizer, T_max=current_sgdr_cycle_epochs, eta_min=new_lr_min)
                if self.loss_cfg.type2 is not None and sgdr_cycle_count >= self.loss_cfg.loss2_start_sgdr_cycle:
                    print("switching to loss type '{}'".format(self.loss_cfg.type2), flush=True)
                    criterion = create_criterion(self.loss_cfg.type2, len(train_data.categories),
                                                 self.loss_cfg.bootstraping_loss_ratio)

            self.optim_summary_writer.add_scalar("sgdr_cycle", sgdr_cycle_count, epoch + 1)

            self.train_summary_writer.add_scalar("loss", train_loss_avg, epoch + 1)
            self.train_summary_writer.add_scalar("mapk", train_mapk_avg, epoch + 1)
            self.val_summary_writer.add_scalar("loss", val_loss_avg, epoch + 1)
            self.val_summary_writer.add_scalar("mapk", val_mapk_avg, epoch + 1)

            epoch_end_time = time.time()
            epoch_duration_time = epoch_end_time - epoch_start_time

            print(
                "[%03d/%03d] %ds, lr: %.6f, loss: %.4f, val_loss: %.4f, acc: %.4f, val_acc: %.4f, ckpt: %d, rst: %d" % (
                    epoch + 1,
                    epochs_to_train,
                    epoch_duration_time,
                    get_learning_rate(optimizer),
                    train_loss_avg,
                    val_loss_avg,
                    train_mapk_avg,
                    val_mapk_avg,
                    int(ckpt_saved),
                    int(sgdr_reset)))

            print('{"chart": "best_val_mapk", "x": %d, "y": %.4f}' % (epoch + 1, global_val_mapk_best_avg))
            print('{"chart": "val_loss", "x": %d, "y": %.4f}' % (epoch + 1, val_loss_avg))
            print('{"chart": "val_mapk", "x": %d, "y": %.4f}' % (epoch + 1, val_mapk_avg))
            print('{"chart": "val_accuracy@1", "x": %d, "y": %.4f}' % (epoch + 1, val_accuracy_top1_avg))
            print('{"chart": "val_accuracy@3", "x": %d, "y": %.4f}' % (epoch + 1, val_accuracy_top3_avg))
            print('{"chart": "val_accuracy@5", "x": %d, "y": %.4f}' % (epoch + 1, val_accuracy_top5_avg))
            print('{"chart": "val_accuracy@10", "x": %d, "y": %.4f}' % (epoch + 1, val_accuracy_top10_avg))
            print('{"chart": "sgdr_cycle", "x": %d, "y": %d}' % (epoch + 1, sgdr_cycle_count))
            print('{"chart": "loss", "x": %d, "y": %.4f}' % (epoch + 1, train_loss_avg))
            print('{"chart": "mapk", "x": %d, "y": %.4f}' % (epoch + 1, train_mapk_avg))
            print('{"chart": "lr_scaled", "x": %d, "y": %.4f}' % (epoch + 1, 1000 * get_learning_rate(optimizer)))
            print('{"chart": "mem_used", "x": %d, "y": %.2f}' % (epoch + 1, psutil.virtual_memory().used / 2 ** 30))
            print('{"chart": "epoch_time", "x": %d, "y": %d}' % (epoch + 1, epoch_duration_time))

            sys.stdout.flush()

            if (sgdr_reset or self.lr_scheduler_cfg == "reduce_on_plateau") and \
                    epoch - epoch_of_last_improval >= self.train_cfg.patience:
                print("early abort due to lack of improval", flush=True)
                break

            if self.optimizer_cfg.max_sgdr_cycles is not None \
                    and sgdr_cycle_count >= self.optimizer_cfg.max_sgdr_cycles:
                print("early abort due to maximum number of sgdr cycles reached", flush=True)
                break

        self.optim_summary_writer.close()
        self.train_summary_writer.close()
        self.val_summary_writer.close()

        train_end_time = time.time()
        print()
        print("Train time: %s" % str(datetime.timedelta(seconds=train_end_time - train_start_time)), flush=True)

    def submission_predictions(self):
        categories = self.train_data.categories
        self.model.load_state_dict(torch.load("{}/model.pth".format(self.model_cfg.output_dir),
                                              map_location=device))
        ensemble_model = Ensemble([self.model])
        #
        submission_df = self.test_data.df.copy()
        predictions, predicted_words = predict(ensemble_model, self.test_data_loader, categories, device, tta=False)
        submission_df["word"] = predicted_words
        np.save("{}/submission_predictions_tta.npy".format(self.model_cfg.output_dir), np.array(predictions))
        submission_df.to_csv("{}/submission_tta.csv".format(self.model_cfg.output_dir), columns=["word"])
        #
        val_set_data_loader = DataLoader(self.val_set, batch_size=64, shuffle=False,
                                         num_workers=self.train_cfg.num_workers, pin_memory=self.train_cfg.pin_memory)
        #
        model = load_ensemble_model(self.model_cfg.output_dir, 3, val_set_data_loader, self.criterion,
                                    self.model_cfg.type, self.model_cfg.image_size,
                                    len(categories), device)
        submission_df = self.test_data.df.copy()
        predictions, predicted_words = predict(model, self.test_data_loader, categories, device, tta=True)
        submission_df["word"] = predicted_words
        np.save("{}/submission_predictions_ensemble_tta.npy".format(self.model_cfg.output_dir), np.array(predictions))
        submission_df.to_csv("{}/submission_ensemble_tta.csv".format(self.model_cfg.output_dir), columns=["word"])

        confusion, _ = calculate_confusion(model, val_set_data_loader, len(categories), device)
        precisions = np.array([confusion[c, c] for c in range(confusion.shape[0])])
        percentiles = np.percentile(precisions, q=np.linspace(0, 100, 10))

        print()
        print("Category precision percentiles:")
        print(percentiles)

        print()
        print("Categories sorted by precision:")
        print(np.array(categories)[np.argsort(precisions)])
