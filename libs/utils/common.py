import yaml
import json
import copy
import os
from collections import OrderedDict, Mapping
from sklearn.model_selection import StratifiedKFold
from glob import glob


def read_categories(file_path):
    with open(file_path) as categories_file:
        return [l.rstrip('\n') for l in categories_file.readlines()]


def value(var):
    if var == -1 or var == 'None':
        return None

def create_if_need(path):
    if not os.path.exists(path):
        os.makedirs(path)


def merge_dicts(*dicts):
    """
    Recursive dict merge.
    Instead of updating only top-level keys,
        dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys.
    """
    assert len(dicts) > 1

    dict_ = copy.deepcopy(dicts[0])

    for merge_dict in dicts[1:]:
        for k, v in merge_dict.items():
            if (k in dict_ and isinstance(dict_[k], dict)
                    and isinstance(merge_dict[k], Mapping)):
                dict_[k] = merge_dicts(dict_[k], merge_dict[k])
            else:
                dict_[k] = merge_dict[k]

    return dict_


def parse_args_config(args, unknown_args, config):
    for arg in unknown_args:
        arg_name, value = arg.split("=")
        arg_name = arg_name[2:]
        value_content, value_type = value.rsplit(":", 1)

        if "/" in arg_name:
            arg_names = arg_name.split("/")
            if value_type == "str":
                arg_value = value_content
            else:
                arg_value = eval("%s(%s)" % (value_type, value_content))

            config_ = config
            for arg_name in arg_names[:-1]:
                if arg_name not in config_:
                    config_[arg_name] = {}

                config_ = config_[arg_name]

            config_[arg_names[-1]] = arg_value
        else:
            if value_type == "str":
                arg_value = value_content
            else:
                arg_value = eval("%s(%s)" % (value_type, value_content))
            args.__setattr__(arg_name, arg_value)
    return args, config


def load_ordered_yaml(
        stream,
        Loader=yaml.Loader, object_pairs_hook=OrderedDict):

    class OrderedLoader(Loader):
        pass

    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))
    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load(stream, OrderedLoader)


def parse_args_uargs(args, unknown_args, dump_config=False):
    args_ = copy.deepcopy(args)

    # load params
    config = {}
    for config_path in args_.config.split(","):
        with open(config_path, "r") as fin:
            if config_path.endswith("json"):
                config_ = json.load(fin, object_pairs_hook=OrderedDict)
            elif config_path.endswith("yml"):
                config_ = load_ordered_yaml(fin)
            else:
                raise Exception("Unknown file format")
        config = merge_dicts(config, config_)

    args_, config = parse_args_config(args_, unknown_args, config)

    # hack with argparse in config
    training_args = config.get("args", None)
    if training_args is not None:
        for key, value in training_args.items():
            arg_value = getattr(args_, key, None)
            if arg_value is None:
                arg_value = value
            setattr(args_, key, arg_value)

    if dump_config and getattr(args_, "logdir", None) is not None:
        save_config(config=config, logdir=args_.logdir)

    return args_, config


def config_parser(config_path):
    config = {}
    with open(config_path, "r") as fin:
        config_ = load_ordered_yaml(fin)
        config = merge_dicts(config, config_)

    return config


def save_config(config, logdir):
    create_if_need(logdir)
    with open("{}/config.json".format(logdir), "w") as fout:
        json.dump(config, fout, indent=2)


def find_sorted_model_files(base_dir):
    return sorted(glob("{}/model-*.pth".format(base_dir)), key=lambda e: int(os.path.basename(e)[6:-4]))


#==================================================
def kfold_split(n_splits, values, classes):
    skf = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=42)
    for train_value_indexes, test_value_indexes in skf.split(values, classes):
        train_values = [values[i] for i in train_value_indexes]
        test_values = [values[i] for i in test_value_indexes]
        yield train_values, test_values


