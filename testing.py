from libs.datasets.prepare_dataset import convert_csv_to_npz
from libs.utils.config_parser import cfg_from_file
from libs.solver import Solver
import numpy as np

if __name__ == '__main__':
    # cfg = cfg_from_file('configs/quickdraw.yml')
    # solver = Solver(cfg)
    with np.load('./storage/quickdraw_doodle/train_simplified_shards/shard-0.npz',
                 allow_pickle=True) as loader:
        data_category = loader["category"]
        data_drawing = loader["drawing"]
        data_recognized = loader["recognized"]
        data_countrycode = loader["countrycode"]