import torch
import torch.nn.functional as F
from libs.utils.network import zero_item_tensor, get_loss_target
from libs.models import create_model, Ensemble
import os
from glob import glob
import numpy as np


def mapk(prediction_logits, categories, topk=3):
    predictions = F.softmax(prediction_logits, dim=1)
    _, predicted_categories = predictions.topk(topk, dim=1, sorted=True)

    apk_v = torch.eq(categories, predicted_categories[:, 0]).float()
    for k in range(1, topk):
        apk_v += torch.eq(categories, predicted_categories[:, k]).float() / (k + 1)

    return apk_v.mean()

def accuracy(prediction_logits, categories, topk=3):
    predictions = F.softmax(prediction_logits, dim=1)
    _, predicted_categories = predictions.topk(topk, dim=1, sorted=True)

    apk_v = torch.eq(categories, predicted_categories[:, 0]).float()
    for k in range(1, topk):
        apk_v += torch.eq(categories, predicted_categories[:, k]).float()

    return apk_v.mean()


def evaluate(model, data_loader, criterion, mapk_topk, device):
    model.eval()

    loss_sum_t = zero_item_tensor(device)
    mapk_sum_t = zero_item_tensor(device)
    accuracy_top1_sum_t = zero_item_tensor(device)
    accuracy_top3_sum_t = zero_item_tensor(device)
    accuracy_top5_sum_t = zero_item_tensor(device)
    accuracy_top10_sum_t = zero_item_tensor(device)
    step_count = 0

    with torch.no_grad():
        for batch in data_loader:
            images, categories, categories_one_hot = \
                batch[0].to(device, non_blocking=True), \
                batch[1].to(device, non_blocking=True), \
                batch[2].to(device, non_blocking=True)

            prediction_logits = model(images)
            # if prediction_logits.size(1) == len(class_weights):
            #     criterion.weight = class_weights
            # using soft boostraping loss
            loss = criterion(prediction_logits, get_loss_target(criterion,
                                categories, categories_one_hot))
            num_categories = prediction_logits.size(1)
            loss_sum_t += loss
            mapk_sum_t += mapk(prediction_logits, categories, topk=min(mapk_topk, num_categories))
            accuracy_top1_sum_t += accuracy(prediction_logits, categories, topk=min(1, num_categories))
            accuracy_top3_sum_t += accuracy(prediction_logits, categories, topk=min(3, num_categories))
            accuracy_top5_sum_t += accuracy(prediction_logits, categories, topk=min(5, num_categories))
            accuracy_top10_sum_t += accuracy(prediction_logits, categories, topk=min(10, num_categories))

            step_count += 1

    loss_avg = loss_sum_t.item() / step_count
    mapk_avg = mapk_sum_t.item() / step_count
    accuracy_top1_avg = accuracy_top1_sum_t.item() / step_count
    accuracy_top3_avg = accuracy_top3_sum_t.item() / step_count
    accuracy_top5_avg = accuracy_top5_sum_t.item() / step_count
    accuracy_top10_avg = accuracy_top10_sum_t.item() / step_count

    return loss_avg, mapk_avg, accuracy_top1_avg, accuracy_top3_avg, accuracy_top5_avg, accuracy_top10_avg

def find_sorted_model_files(base_dir):
    return sorted(glob("{}/model-*.pth".format(base_dir)), key=lambda e: int(os.path.basename(e)[6:-4]))


def predict(model, data_loader, categories, device, tta=False):
    categories = np.array([c.replace(" ", "_") for c in categories])

    model.eval()

    all_predictions = []
    predicted_words = []
    with torch.no_grad():
        for batch in data_loader:
            images = batch[0].to(device, non_blocking=True)

            if tta:
                predictions1 = F.softmax(model(images), dim=1)
                predictions2 = F.softmax(model(images.flip(3)), dim=1)
                predictions = 0.5 * (predictions1 + predictions2)
            else:
                predictions = F.softmax(model(images), dim=1)

            _, prediction_categories = predictions.topk(3, dim=1, sorted=True)

            all_predictions.extend(predictions.cpu().data.numpy())
            predicted_words.extend([" ".join(categories[pc.cpu().data.numpy()])
                                    for pc in prediction_categories])

    return all_predictions, predicted_words


def calculate_confusion(model, data_loader, num_categories, device, scale=True):
    confusion = np.zeros((num_categories, num_categories), dtype=np.float32)

    model.eval()

    all_predictions = []
    with torch.no_grad():
        for batch in data_loader:
            images, categories = \
                batch[0].to(device, non_blocking=True), \
                batch[1].to(device, non_blocking=True)

            predictions = F.softmax(model(images), dim=1)
            _, prediction_categories = predictions.topk(3, dim=1, sorted=True)

            for bpc, bc in zip(prediction_categories[:, 0], categories):
                confusion[bpc, bc] += 1

            all_predictions.extend(predictions.cpu().data.numpy())

    if scale:
        for c in range(confusion.shape[0]):
            category_count = confusion[c, :].sum()
            if category_count != 0:
                confusion[c, :] /= category_count

    return confusion, all_predictions
