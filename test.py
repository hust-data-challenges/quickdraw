
def passwordCracker(passwords, loginAttempt, f):
    if loginAttempt in passwords:
        return loginAttempt
    # Write your code here
    if loginAttempt in f.keys():
        return f[loginAttempt]

    res = "WRONG PASSWORD"
    for password in passwords:
        if loginAttempt[:len(password)] == password:
            r = passwordCracker(passwords, loginAttempt[len(password):], f)
            if r != "WRONG PASSWORD":
                res = password + ' ' + r
            else:
                res = "WRONG PASSWORD"
    if res != "WRONG PASSWORD":
        f[loginAttempt] = res
    return res


def newPassWordCraker(passwords, loginAttemmt):
    print(passwordCracker(['a', 'aa', 'aaa', 'aaaa', 'aaaaa', 'aaaaaa',
                       'aaaaaaa', 'aaaaaaaa', 'aaaaaaaaa', 'aaaaaaaaaa'],
                'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab', {}))