import numpy as np

CATEGORIES = ['passport', 'bird', 'cup', 'firetruck', 'teapot', 'camouflage', 'diving board', 'radio', 'sweater',
              'snake', 'airplane', 'snail', 'dumbbell', 'rake', 'dolphin', 'frog', 'helmet', 'helicopter', 'cannon',
              'parrot', 'stove', 'grass', 'skateboard', 'ear', 'belt', 'pants', 'washing machine', 'clarinet',
              'light bulb', 'tractor', 'necklace', 'mouse', 'lantern', 'spoon', 'duck', 'elbow', 'laptop', 'arm',
              'rabbit', 'dragon', 'jacket', 'toothpaste', 'waterslide', 'leaf', 'angel', 'camel', 'mug', 'ladder',
              'bulldozer', 'coffee cup', 'hockey stick', 'finger', 'stitches', 'garden', 'calculator', 'camera',
              'carrot', 'paint can', 'stop sign', 'screwdriver', 'onion', 'stairs', 'cat', 'picture frame', 'ocean',
              'monkey', 'floor lamp', 'book', 'raccoon', 'cruise ship', 'remote control', 'pig', 'canoe', 'shoe',
              'squirrel', 'hamburger', 'drums', 'harp', 'telephone', 'panda', 'sea turtle', 'asparagus', 'spreadsheet',
              'triangle', 'car', 'fork', 'butterfly', 'barn', 'bracelet', 'motorbike', 'envelope', 'mermaid', 'steak',
              'giraffe', 'pizza', 'power outlet', 'van', 'paintbrush', 'lobster', 'brain', 'kangaroo', 'church',
              'couch', 'mosquito', 'calendar', 'see saw', 'eyeglasses', 'yoga', 'tooth', 'hexagon', 'peanut', 'line',
              'house plant', 'smiley face', 'spider', 'foot', 'bottlecap', 'skyscraper', 'bat', 'jail', 'purse',
              'pillow', 'toilet', 'bridge', 'rhinoceros', 'streetlight', 'bread', 'river', 'binoculars', 'pliers',
              'door', 'television', 'sheep', 'keyboard', 'peas', 'The Mona Lisa', 'pond', 'fence', 'tree', 'sink',
              'bush', 'hockey puck', 'eraser', 'knee', 'chair', 'snorkel', 'saxophone', 'hedgehog', 'ice cream',
              'house', 'feather', 'diamond', 'mountain', 'toaster', 'matches', 'watermelon', 'hot air balloon',
              'square', 'sandwich', 'broccoli', 'pool', 'flamingo', 'tornado', 'baseball', 'cactus', 'pencil', 'ant',
              'marker', 'piano', 'key', 'basketball', 'cookie', 'lightning', 'snowflake', 'elephant', 'blueberry',
              'tent', 'ceiling fan', 'traffic light', 'hat', 'school bus', 'suitcase', 'garden hose', 'cello', 'crayon',
              'mushroom', 'paper clip', 'squiggle', 'sun', 'bench', 'face', 'truck', 'octagon', 'bowtie', 'cloud',
              'teddy-bear', 'flower', 'beach', 'crown', 'whale', 'hand', 'bed', 'sailboat', 'banana', 'stereo',
              'octopus', 'fan', 'axe', 'bear', 'shorts', 'bee', 'lion', 'tennis racquet', 'dresser', 'eye', 'train',
              'sock', 'penguin', 'The Eiffel Tower', 'snowman', 'bathtub', 'skull', 'castle', 'roller coaster', 'leg',
              'bandage', 'map', 'hospital', 'hot dog', 'nose', 'bicycle', 'strawberry', 'guitar', 'blackberry',
              'popsicle', 'hurricane', 'backpack', 'dog', 'soccer ball', 'toothbrush', 'violin', 'underwear', 'basket',
              'flip flops', 'beard', 'palm tree', 'apple', 'bus', 'megaphone', 'rollerskates', 'police car',
              'animal migration', 'wristwatch', 'circle', 't-shirt', 'mailbox', 'flying saucer', 'zebra', 'shovel',
              'hourglass', 'cake', 'nail', 'oven', 'saw', 'tiger', 'horse', 'alarm clock', 'postcard', 'compass',
              'dishwasher', 'fireplace', 'cooler', 'bucket', 'birthday cake', 'The Great Wall of China', 'speedboat',
              'anvil', 'baseball bat', 'mouth', 'sword', 'grapes', 'sleeping bag', 'fish', 'clock', 'shark', 'pear',
              'cow', 'trumpet', 'pickup truck', 'chandelier', 'candle', 'microwave', 'cell phone', 'rainbow',
              'headphones', 'owl', 'windmill', 'rain', 'golf club', 'goatee', 'swan', 'lighthouse', 'vase', 'hammer',
              'hot tub', 'table', 'zigzag', 'wine bottle', 'parachute', 'flashlight', 'broom', 'swing set',
              'wine glass', 'submarine', 'wheel', 'crab', 'crocodile', 'scorpion', 'scissors', 'string bean',
              'trombone', 'fire hydrant', 'ambulance', 'lipstick', 'umbrella', 'moustache', 'lollipop', 'star',
              'campfire', 'potato', 'stethoscope', 'frying pan', 'toe', 'donut', 'pineapple', 'drill', 'microphone',
              'boomerang', 'moon', 'computer']

COUNTRIES = [
    'AD', 'AE', 'AF', 'AG', 'AI', 'AL', 'AM', 'AN', 'AO', 'AR', 'AS', 'AT', 'AU', 'AW', 'AX', 'AZ', 'BA', 'BB', 'BD',
    'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BM', 'BN', 'BO', 'BR', 'BS', 'BT', 'BU', 'BW', 'BY', 'BZ', 'CA', 'CD', 'CF',
    'CG', 'CH', 'CI', 'CL', 'CM', 'CN', 'CO', 'CR', 'CV', 'CW', 'CX', 'CY', 'CZ', 'DE', 'DJ', 'DK', 'DM', 'DO', 'DZ',
    'EC', 'EE', 'EG', 'ES', 'ET', 'FI', 'FJ', 'FM', 'FO', 'FR', 'GA', 'GB', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GL',
    'GM', 'GN', 'GP', 'GR', 'GT', 'GU', 'GY', 'HK', 'HN', 'HR', 'HT', 'HU', 'ID', 'IE', 'IL', 'IM', 'IN', 'IQ', 'IR',
    'IS', 'IT', 'JE', 'JM', 'JO', 'JP', 'KE', 'KG', 'KH', 'KN', 'KR', 'KW', 'KY', 'KZ', 'LA', 'LB', 'LC', 'LI', 'LK',
    'LR', 'LS', 'LT', 'LU', 'LV', 'LY', 'MA', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP',
    'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ', 'NC', 'NF', 'NG', 'NI', 'NL', 'NO', 'NP', 'NZ', 'OM',
    'PA', 'PE', 'PF', 'PG', 'PH', 'PK', 'PL', 'PM', 'PR', 'PS', 'PT', 'PW', 'PY', 'QA', 'RE', 'RO', 'RS', 'RU', 'RW',
    'SA', 'SC', 'SE', 'SG', 'SI', 'SJ', 'SK', 'SM', 'SN', 'SO', 'SR', 'SS', 'ST', 'SV', 'SX', 'SZ', 'TC', 'TG', 'TH',
    'TJ', 'TL', 'TM', 'TN', 'TO', 'TR', 'TT', 'TW', 'TZ', 'UA', 'UG', 'US', 'UY', 'UZ', 'VC', 'VE', 'VG', 'VI', 'VN',
    'VU', 'WS', 'YE', 'YT', 'ZA', 'ZM', 'ZW', 'ZZ', None]

COUNTRY_INDEX_MAP = {
    c: COUNTRIES.index(c) for c in COUNTRIES
}

