#!/usr/bin/env bash

pip install kaggle
apt install pv

echo '{"username":"tuanhleo","key":"8039611d3e9d9cd2288a1467bcb291d7"}' > kaggle.json
mkdir -p ~/.kaggle/
cp kaggle.json ~/.kaggle/
chmode 600 ~/.kaggle/kaggle.json
cat ~/.kaggle/kaggle.json

if [ ! -d "./storage" ]
then
   mkdir storage
   echo "create folder storage"
fi

if [ ! -d "./storage/quickdraw_doodle" ]
then
   mkdir storage/quickdraw_doodle
   echo "create folder storage/quickdraw_doodle"
fi

cd storage/quickdraw_doodle

kaggle competitions download -c quickdraw-doodle-recognition -f sample_submission.csv
kaggle competitions download -c quickdraw-doodle-recognition -f test_simplified.csv
kaggle competitions download -c quickdraw-doodle-recognition -f test_raw.csv
kaggle competitions download -c quickdraw-doodle-recognition -f train_simplified.zip

mkdir train_simplified
unzip -o train_simplified.zip -d train_simplified | pv -l >/dev/null

#rm train_simplified.zip
cd ../..

echo 'download and unzip processing is compelete'
