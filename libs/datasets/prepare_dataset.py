import os
from glob import glob
from tqdm import tqdm
import numpy as np
from libs.utils.common import read_categories
from multiprocessing import Pool
import math
import pandas as pd
import shutil
from libs.constant import CATEGORIES


def prepare_shards(num_shards):
    if os.path.isdir("storage/quickdraw_doodle/train_simplified_shards"):
        shutil.rmtree("storage/quickdraw_doodle/train_simplified_shards")
    os.makedirs("storage/quickdraw_doodle/train_simplified_shards")

    # categories = read_categories('storage/quickdraw_doodle/categories.txt')
    categories = CATEGORIES

    for category in tqdm(categories):
        csv_file_name = "storage/quickdraw_doodle/train_simplified/{}.csv".format(category)

        print("processing file '{}'".format(csv_file_name), flush=True)

        df = pd.read_csv(csv_file_name, index_col="key_id")

        shard_size = math.ceil(len(df) / num_shards)
        indexes = df.index.values
        np.random.shuffle(indexes)

        for s in range(num_shards):
            start = s * shard_size
            end = min(start + shard_size, len(df))
            shard_df = df[df.index.isin(indexes[start:end])]
            shard_file_name = "storage/quickdraw_doodle/train_simplified_shards/shard-{}.csv".format(s)
            write_csv_header = not os.path.isfile(shard_file_name)
            with open(shard_file_name, "a") as shard_file:
                shard_df.to_csv(shard_file, header=write_csv_header)

def csv_to_npz(csv_file_name):
    print("reading file '{}'".format(csv_file_name), flush=True)

    # categories = read_categories("storage/quickdraw_doodle/categories.txt")
    categories = CATEGORIES

    df = pd.read_csv(
        csv_file_name,
        index_col="key_id")

    df['word'] = df['word'].swifter.apply(lambda word: categories.index(word))
    df['drawing'] = df['drawing'].swifter.apply(lambda drawing: np.array(eval(drawing)))
    df = df.rename(columns={"word": "category"})

    key_id = np.array(df.index.values, dtype=np.int64)
    drawing = np.array(df.drawing.values, dtype=np.object)
    category = np.array(df.category.values, dtype=np.int16)
    recognized = np.array(df.recognized.values, dtype=np.bool)
    countrycode = np.array(df.countrycode.values, dtype=np.object)

    npz_file_name = csv_file_name[:-4] + ".npz"
    print("writing file '{}'".format(npz_file_name), flush=True)
    np.savez_compressed(
        npz_file_name,
        key_id=key_id,
        drawing=drawing,
        category=category,
        recognized=recognized,
        countrycode=countrycode)

    return None

def convert_csv_to_npz():
    csv_file_names = glob("storage/quickdraw_doodle/train_simplified_shards/*.csv")

    with Pool(5) as pool:
        pool.map(csv_to_npz, csv_file_names)

if __name__ == '__main__':
    # prepare_shards(50)
    convert_csv_to_npz()