from . import FocalLoss, CceCenterLoss, CrossEntropyLoss


def create_criterion(loss_type, num_classes, bootstraping_loss_ratio):
    if loss_type == "cce":
        criterion = CrossEntropyLoss()
    elif loss_type == "focal":
        criterion = FocalLoss()
    elif loss_type == "center":
        criterion = CceCenterLoss(num_classes=num_classes, alpha=0.5)
    else:
        raise Exception("Unsupported loss type: '{}".format(loss_type))
    return criterion